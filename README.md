> Pour le cas d'application entre Garradin et HelloAsso (et autres), suivre ce lien : https://gitlab.com/jrd10/garradin-helloasso-connecteur

# Architecture logicielle et les API

Principale source : Présentation de Guillaume, le vendredi 9 avril 2021, Oisux.


## 12 software architecture quality attributes (Syndicode)

- https://syndicode.com/blog/12-software-architecture-quality-attributes/

1. **Performance** – shows the response of the system to performing certain actions for a certain period of time.

2. **Interoperability** is an attribute of the system or part of the system that is responsible for its operation and the transmission of data and its exchange with other external systems.

3. **Usability** is one of the most important attributes, because, unlike in cases with other attributes, users can see directly how well this attribute of the system  worked out.

4. **Reliability** is an attribute of the system responsible for the ability to continue to operate under predefined conditions.

5. **Availability** is part of reliability and is expressed as the ratio of the available system time to the total working time.

6. **Security** is responsible for the ability of the system to reduce the likelihood of malicious or accidental actions as well as the possibility of theft or loss of information.

7. **Maintainability** is the ability of the system to support changes.

8. Modifi****ability determines how many common changes need to be made to the system to make changes to each individual item.

9. **Testability** shows how well the system allows performing tests, according to predefined criteria.

10. **Scalability** is the ability of the system to handle load increases without decreasing performance, or the possibility to rapidly increase the load.

11. **Reusability** is a chance of using a component or system in other components/systems with small or no change.

12. **Supportability** is the ability of the system to provide useful information for identifying and solving problems.


## Techniques pour améliorer l'accès aux systèmes

- Les systèmes échangent à travers des requêtes. Il faut raccourcir le temps entre ces échanges.

1. Réplication de BD : Les esclaves que pour la lecture et master pour l'écriture

2. Cache en mémoire : Redis techno du cache en mémoire

3. CDN Content Delivery Network : du cache près de la demande :) 
- Un cache géolocalisé, avec des serveurs... TTL Time to Live


4. Message Queue... Les demandes sont traitées dans une file d'attente (en milli ou seconde)

5. Kibana de Elastic : https://www.elastic.co/fr/kibana 
- Mesure l'activité d'un processus, composant... Tableaux de bord
- Permet le suivi de la charge de travail liée à la recherche ou comprendre le flux des requêtes dans vos applications


6.  le charding : on répartit les données dans plusieux BD : clé de distribution

7. Cloud Computing 
    - Infrastructure As A Services (IAAS)
    - Platform As A Services (PAAS) : Conteneur. 
    - Docker (Google) Open source le 13 mars 2013
    - Outils Open source de virtualisation : LXC, OpenVz... PROXMOX 

## Vertical vs horizontal scaling

- Vertical scaling : de plus gros serveurs

- Horizontal scaling : plusieurs machines : Load balancer, Géoroutage, orchestration (Kubernetes)

## Les API

### API Rest
- API Web : - des appels en http.
- L’acronyme REST (REpresentational State Transfer) a été inventé par Roy Fielding, un scientifique américain qui a beaucoup contribué dans les spécifications du protocole HTTP.
- Mode client serveur
- Gérer en local... grâce au GET

#### Les verbe GET (Courtesy de http://www.illustradata.com/api-rest/)

- : Fondé sous le principe KISS (Keep it simple, stupid)
    
##### POST

- Permet de **rajouter des entités à une ressource**. Les **données sont envoyées dans le corps de la requête**.

- Par défaut, une requête POST intègre le format Content-Type:application/x-www-form-urlencoded. Ce **format est déconseillé** avec les APIs REST c**ar il ne correspond pas aux formats de réponse utilisés (JSON, XML)**.

`curl -H "Content-Type: application/json" -X POST -d '{"immat":"aa111aa", "marque": "peugeot"}' https://api.site.fr/vehicules`

##### PUT

- Permet de **modifier les données d’une entité dans une ressource**.

- L’url doit indiquer l’identifiant de l’entité. Si l’identifiant est inexistant, l’entité devrait être créée.

`curl -H -X PUT -d '{"immat":"aa111aa", "marque": "peugeot"}' https://api.site.fr/vehicules/1`

##### PATCH (OPTIONNEL)

- Permet de **modifier en partie les données d’une entité dans une ressource**.

- Ce verbe ne fait pas partie des opérations de base pour la persistance des données (CRUD) mais **pourrait être considéré comme une extension au verbe PUT** lui permettant de simplifier les mises à jour partielles.

`curl -H -X PATCH -d '{"immat":"aa222aa"}' https://api.site.fr/vehicules/1`

##### DELETE

- Permet de **supprimer une entité dans une ressource**.

`curl -H -X DELETE https://api.site.fr/vehicules/1`

##### LES CODES RETOUR HTTP D’API REST

Les codes retours permettent de donner un statut aux réponses renvoyées par l’API.

![Les langages utilisés par les API](http://i0.wp.com/www.illustradata.com/wp-content/uploads/2017/05/Illu_API_REST_1.png?w=692 "Courtezy de http://www.illustradata.com/api-rest/")

- json / YAML (plus dépoyé)
- XML trop lourd
- Python / JSON : import json

##### Technique de sécurisation des API


1. HTTP Basic Authentication 
    - La plus simple. Consiste à envoyer le login et le mot de passe (en https) dans l’entête de chaque requête.

2. Oauth
    - Oauth est un protocole de délégation d’autorisation **nécessitant un serveur tiers comme fournisseur d’accès**.

    - Malgré la complexité de sa mise en place, ce protocole est très apprécié pour sa sécurité.

    - Il existe deux versions: 
        - OAuth 1.0 : sans https, difficile à mettre en place

        - Oauth 2.0 : avec https, plus facile à mettre en place. Néanmoins, cette version est moins sécurisée que Oauth 1.0.  


- OpenID
    - De base, OpendID est un système d’identification en mode SSO. Il permet à un client de se connecter auprès de plusieurs sites sans devoir créer un compte à chaque fois.
    
    - Le mode de fonctionnement ressemble à celui de Oauth. Il faut un fournisseur d’identité (OpendID providers) pour établir un lien de confiance entre le client et le serveur. Souvent, le fournisseur d’identité est un grand nom du Web (facebook, twitter, google, …).

    - D’autre part, cette solution ne permet pas de gérer l’authentification. De ce fait, il existe une deuxième version (OpenID Connect) qui intègre une sous couche Oauth.

##### Le versionning

- Le versionning dans les API n'est pas aisé. Il est recommandé de plutôt recréer de nouveaux services plutôt que de les modifier.

- Trois types de versionning :
    - Via l’URL – Consiste à rajouter le numéro de version dans l’URL de chaque ressource. 
    - Via l’entête HTTP – S’appuie sur des mécanismes d’identification de version depuis l’entête de la requête (AcceptHeaderVersioning).
    - Via les paramètres – Dans ce type de gestion, la version est passée dans les paramètres de la requête.

##### API Management ou API Gateway

- Kong– Solution open-source basée sur un système de plugins. 
- Ty – Solution open-source facilement intégrable et disponible en version dockerisée.
- https://gravitee.io/ – Solution open-source en Java. 
- Zuul – Solution open-source en Java.
- API Umbrella – Solution open-source en Java.


## API RPC
- Utilisé dans les systèmes clients-serveurs 
- https://fr.wikipedia.org/wiki/Remote_procedure_call

## SOAP
- SOAP (ancien acronyme de Simple Object Access Protocol) est un protocole d'échange d'information structurée dans l'implémentation de services web bâti sur XML. Utilisé dans le cadre d'architecture de type SOA (Service Oriented Architecture)
- https://fr.wikipedia.org/wiki/SOAP

## OAuth 2.0
- L'authentification est déléguée
- https://zestedesavoir.com/articles/1616/comprendre-oauth-2-0-par-lexemple/ 
- https://developer.okta.com/blog/2018/05/24/what-is-the-oauth2-implicit-grant-type

- Voir la vidéo GraphikArt (du php) : https://grafikart.fr/tutoriels/oauth2-php-google-1171

## OpenAPI : Swagger
- La société Swagger (https://swagger.io/) a écrit les spécifications d'OpenAPI, 
- HelloAsso travaille sur Swagger :  Guillaumen : bien documenté
    - https://api.helloasso.com/v5/swagger/ui/index
- token JWT...

- https://en.wikipedia.org/wiki/Open_API
- https://en.wikipedia.org/wiki/OpenAPI_Specification

- Génère le code automatiquement : Faire l'essai en Python :)
- Recherche d'un outil : generate api client python swagger 
- Generating server stubs (site pour tests) and client SDKs for any API : Swagger Codegen : https://swagger.io/tools/swagger-codegen/
- SwaggerHub : https://swagger.io/tools/swaggerhub/

## Architecture logicielle

### Architecture d'événements
- Event-driven architecture

### MikroKernel
- Coeur + extension
- Comme Garradin, l'équipe s'occupe du Core software, les contributions par extensions

### curl : le couteau Suisse de l'admin

### Static web site
- Gatsby : gatsbyjs.com

### Monolithique vs Distribuée
- Architecture n-tiers

## L'urbanisation de Système d'information

- Ici, c'est l'organisation entière de l'informatique qui est étudiée :)
- https://fr.wikipedia.org/wiki/Urbanisation_(informatique)
- https://fr.wikipedia.org/wiki/M%C3%A9tamod%C3%A8le_d%27urbanisme
- Voir mon projet sur GitLab, appliqué à la comptabilité d'une association avec Garradin : 
    - https://gitlab.com/jrd10/urbanisation-si

- Exemple de description (simplifiée) basée sur le méta-modèle de Christophe Longépé
![Description de la "compta" d'une association](https://gitlab.com/jrd10/urbanisation-si/-/raw/master/modeles/Urba-SI-Anonyme_Comptabilite-Sauvegarde_v01.png "Diagramme avec diagrams.net")


## Licence
Dans le domaine public : CC0
